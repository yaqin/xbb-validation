"""
Wrappers for basic matplotlib figures.
"""

import os

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import FuncFormatter
from matplotlib.figure import Figure
from matplotlib import gridspec
from matplotlib.artist import setp
from matplotlib.ticker import AutoMinorLocator

import math

def helvetify():
    """
    Load 'Helvetica' default font (may be Arial for now)
    """
    from matplotlib import rc
    # 'Comic Sans MS', 'Trebuchet MS' works, Arial works in png...
    rc('font',**{'family':'sans-serif','sans-serif':['Arial']})

def log_formatting(value, pos):
    """
    This is for use with the FuncFormatter.

    It writes 1 and 10 as plane numbers, everything else as exponential.
    """
    roundval = round(value)
    if roundval == 1:
        base = 1
        exp = ''
    elif roundval == 10:
        base = 10
        exp = ''
    else:
        base = 10
        exp = round(math.log(value,10))
    return r'{}$^{{\mathdefault{{ {} }} }}$'.format(base, exp)

def xlabdic(fontsize=20):
    # options for the x axis
    return dict(ha='right', x=0.98, fontsize=fontsize)
def ylabdic(fontsize=20):
    # options for the y axis
    return dict(ha='right', y=0.98, fontsize=fontsize)

def atlas_style(ax, fontsize=18):
    where = {x:True for x in ['bottom','top','left','right']}
    ax.tick_params(which='both', direction='in', labelsize=fontsize, **where)
    ax.tick_params(which='minor', length=6)
    ax.tick_params(which='major', length=12)
    ax.grid(True, which='major', alpha=0.10)
    ax.xaxis.set_minor_locator(AutoMinorLocator(5))
    ax.yaxis.set_minor_locator(AutoMinorLocator(5))

def add_atlas_label_in_box(ax, x, y, internal=True, size=28):
    post = 'Internal' if internal else 'Simulation Preliminary'
    text_fd = dict(ha='left', va='top', size=size)
    bbox = dict(boxstyle='round', fc='w')
    atlas_fd = dict(weight='bold', style='italic', bbox=bbox, **text_fd)
    text = 'ATLAS' + ' '*int(1.7*(len(post)+1))
    ax.text(x, y, text, transform=ax.transAxes, **atlas_fd)
    ax.text(x+0.2, y, post, transform=ax.transAxes, **text_fd)

def add_sim_info_in_box(ax, x, y, size=18):
    props = dict(boxstyle='round', facecolor='w')
    text = (
        r'$\sqrt{s} = 13\,{\rm TeV}$' + '\n'
        'Preselection:\n'
        r'$|\eta_{\rm J}| < $ 2.0' + '\n'
        r'$76 < m_{J}\,/\,{\rm GeV} < 146$' + '\n'
    )[:-1]
    ax.text(x, y, text, size=size,
            transform=ax.transAxes, bbox=props, va='bottom', ha='left')

def add_atlas_label(ax, x=0.05, y=0.91, internal=True, vshift=0.08,
                    add_kinematic_info=False,
                    label_break_ratio_hack=1.0):
    text_fd = dict(ha='left', va='center')
    atlas_fd = dict(weight='bold', style='italic', size=28, **text_fd)
    internal_fd = dict(size=28, **text_fd)
    info_df = dict(size=18, **text_fd)
    shift = 0.2 * label_break_ratio_hack
    ax.text(x,y,'ATLAS', fontdict=atlas_fd,transform=ax.transAxes)
    ax.text(x+shift,y, 'Internal' if internal else 'Simulation Preliminary',
            fontdict=internal_fd,transform=ax.transAxes)
    ax.text(x,y-vshift, r'$\sqrt{s}=13$ TeV',
            fontdict=info_df,transform=ax.transAxes)
    if add_kinematic_info:
        ax.text(x,y-2*vshift, r'$p_{\rm T}^{\rm J} > 250$ GeV',
                fontdict=info_df,transform=ax.transAxes)
        ax.text(x,y-3*vshift, r'$|\eta_{\rm J}| < $ 2.0',
                fontdict=info_df,transform=ax.transAxes)
        ax.text(x,y-4*vshift, r'$76 < m_{J}\,/\,{\rm GeV} < 146$',
                fontdict=info_df,transform=ax.transAxes)

def sad_atlas_label(ax, x=0.05, y=0.91, vshift=0.08,
                    label_break_ratio_hack=0.8):
    text_fd = dict(ha='left', va='center')
    atlas_fd = dict(weight='bold', style='italic', size=28, **text_fd)
    internal_fd = dict(size=28, **text_fd)
    info_df = dict(size=18, **text_fd)
    shift = 0.2 * label_break_ratio_hack
    ax.text(x,y,'ATLAS', fontdict=atlas_fd,transform=ax.transAxes)
    ax.text(x+shift,y, 'sort of Internal',
            fontdict=internal_fd,transform=ax.transAxes)
    ax.text(x,y-vshift, r'$\sqrt{s}=13$ TeV',
            fontdict=info_df,transform=ax.transAxes)
    ax.text(x,y-2*vshift, r'$p_{\rm T}^{\rm J} > 250$ GeV',
            fontdict=info_df,transform=ax.transAxes)
    ax.text(x,y-3*vshift, r'$76 < m_{J}\,/\,{\rm GeV} < 146$',
            fontdict=info_df,transform=ax.transAxes)



def add_kinematic_acceptance(ax, pt_range=None, eff=None, offset=0.08):
    x=0.05
    y=0.05
    kin_args = dict(transform=ax.transAxes, size=18, ha='left', va='bottom')
    ax.text(x,y, r'$76 < m_{J}\,/\,{\rm GeV} < 146$', **kin_args)
    y+=offset
    if pt_range is not None:
        ptj = r'p_{\rm T}^{\rm J}'
        gev = r'\,{\rm GeV}'
        pt_min, pt_max = ('{:.0f}'.format(x*1e-3) for x in pt_range)
        if math.isinf(pt_range[1]):
            pt_string = f'${ptj} > {pt_min}{gev}$'
        else:
            pt_string = f'${pt_min}{gev} < {ptj}\,/\,{gev} < {pt_max}$'
        ax.text(x,y,pt_string, **kin_args)
        y+=offset
    ax.text(x,y,r'$|\eta_{\rm J}| < 2.0$', **kin_args)
    y += offset
    ax.text(x, y, 'Preselection:', **kin_args)
    y += offset
    if eff is not None:
        eff_num = '{:.1f}'.format(eff)
        eff_string = r'$\epsilon_{\rm Higgs}$ = ' + eff_num
        ax.text(x, y, eff_string, **kin_args)

def log_style(ax):
    ax.set_yscale('log')
    ax.yaxis.set_major_formatter(FuncFormatter(log_formatting))
    ax.grid(True, which='minor', axis='y', alpha=0.03)


class Canvas:
    default_name = 'test.pdf'
    def __init__(self, out_path=None, figsize=(10,8), ext=None,
                 fontsize=18):
        self.fig = Figure(figsize)
        self.fig.set_tight_layout(True)
        self.canvas = FigureCanvas(self.fig)
        self.ax = self.fig.add_subplot(1,1,1)
        self.out_path = str(out_path)
        self.ext = ext

        atlas_style(self.ax, fontsize=fontsize)

    def save(self, out_path=None, ext=None):
        output = out_path or self.out_path
        assert output, "an output file name is required"
        out_dir, out_file = os.path.split(output)
        if ext:
            out_file = '{}.{}'.format(out_file, ext.lstrip('.'))
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)
        self.canvas.print_figure(output, bbox_inches='tight')

    def __enter__(self):
        if not self.out_path:
            self.out_path = self.default_name
        return self
    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        self.save(self.out_path, ext=self.ext)
        return True

class CanvasWithRatio:
    default_name = 'test.pdf'
    def __init__(self, out_path=None, figsize=(10,8), ext=None):
        self.fig = Figure(figsize)
        self.canvas = FigureCanvas(self.fig)
        gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
        self.ax = self.fig.add_subplot(gs[0])
        self.ax2 = self.fig.add_subplot(gs[1],sharex=self.ax)

        self.fig.subplots_adjust(hspace=0.05)
        self.out_path = str(out_path)
        self.ext = ext

        atlas_style(self.ax)
        atlas_style(self.ax2)

        # hide the x tick lables on the upper plot
        setp(self.ax.get_xticklabels(), visible=False)

    def save(self, out_path=None, ext=None):
        output = out_path or self.out_path
        assert output, "an output file name is required"
        out_dir, out_file = os.path.split(output)
        if ext:
            out_file = '{}.{}'.format(out_file, ext.lstrip('.'))
        if out_dir and not os.path.isdir(out_dir):
            os.makedirs(out_dir)

        self.canvas.print_figure(output, bbox_inches='tight')

    def __enter__(self):
        if not self.out_path:
            self.out_path = self.default_name
        return self
    def __exit__(self, extype, exval, extb):
        if extype:
            return None
        self.save(self.out_path, ext=self.ext)
        return True
