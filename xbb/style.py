DISCRIMINANT_NAME_MAP = {
    'xbb_anti_qcd': r'XbbScore $p_{H} / p_{jet}$',
    'xbb_anti_top': r'XbbScore $p_{H} / p_{top}$',
    'xbb_mixed': r'XbbScore $p_{H} / (p_{top} + p_{jet})$',
    'xbb_top_vs_qcd': r'XbbScore Top Tagger',
    'dl1r': r'2 VR DL1r',
    'dl1r_FR': r'2 $R=0.2$ DL1r',
    'dl1': r'2 VR DL1',
    'mv2': r'2 VR MV2',
    'mv2_FR': r'2 $R=0.2$ MV2',
    'jss_top_vs_qcd': r'DNN Top Tagger',
    'tau32_top_vs_qcd': r'$\tau_{32}$',
    'Hbb_anti_qcd': r'$D_{\rm Xbb}$ $p_{H} / p_{jet}$',
    'Hbb_anti_top': r'$D_{\rm Xbb}$ $p_{H} / p_{top}$',
    'Hbb_mixed': r'$D_{\rm Xbb}$, $f_{\rm top}=0.5$',
    'Hbb_025': r'$D_{\rm Xbb}$, $f_{\rm top}=0.25$',
    'eta': r'$\eta$',
    'all': 'All Jets'
}
DISCRIMINANT_NAME_MAP_SHORT = {
    'dl1': 'DL1',
    'dl1r': 'DL1r',
    'dl1r_FR': 'DL1r FR',
    'mv2': 'MV2',
    'mv2_FR': 'MV2 FR',
}
DISCRIMINANT_COLOR_MAP = {
    'xbb_anti_qcd': 'blue',
    'xbb_anti_top': 'orange',
    'xbb_mixed': 'darkred',
    'Hbb_anti_qcd': 'blue',
    'Hbb_anti_top': 'orange',
    'Hbb_mixed': 'red',
    'Hbb_025': 'deepskyblue',
    'dl1r': 'green',
    'dl1r_FR': 'darkgreen',
    'dl1': 'green',
    'mv2': 'gray',
    'mv2_FR': 'darkgray',
    'jss_top_vs_qcd': 'red',
    'tau32_top_vs_qcd': 'gray',
    'xbb_top_vs_qcd': 'blue',
    'all': 'red',
}

DISCRIMINANT_LS_MAP = {
    'xbb_anti_qcd': '-',
    'xbb_anti_top': '-',
    'xbb_mixed': '-',
    'Hbb_anti_qcd': '-',
    'Hbb_anti_top': '-',
    'Hbb_mixed': '-',
    'Hbb_025': '-',
    'dl1r': '-',
    'dl1r_FR': '--',
    'dl1': '-',
    'mv2': '-',
    'mv2_FR': '--',
    'jss_top_vs_qcd': '-',
    'tau32_top_vs_qcd': '-',
    'xbb_top_vs_qcd': '-',
}
PROCESS_NAME_MAP = {
    'dijet': 'Multijet',
    'top': 'Top',
    'higgs': 'Higgs'
}
RATIO_ZOOM_MAP = {
    ('250_inf', 'multijet'): (0.5, 1.8),
    ('500_inf', 'multijet'): (0.75, 2.5),
    ('750_inf', 'multijet'): (0.5, 2.5),
}
PROCESS_COLOR_MAP = {
    'higgs': 'red',
    'top': 'green',
    'dijet': 'blue'
}
