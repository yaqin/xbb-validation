#!/usr/bin/env python3

"""
Draw roc 2d roc curves in pt vs eff space
"""

from argparse import ArgumentParser
from glob import glob
import os
from pathlib import Path

import numpy as np
from h5py import File
from scipy.interpolate import LinearNDInterpolator
from scipy.spatial import Delaunay

from xbb.mpl import Canvas, helvetify, xlabdic, ylabdic
from xbb.mpl import add_atlas_label_in_box, add_sim_info_in_box
from xbb.style import (
    DISCRIMINANT_NAME_MAP, PROCESS_NAME_MAP, DISCRIMINANT_NAME_MAP_SHORT)
from xbb.common import ap_default_args as apd

from matplotlib.colors import LogNorm, Normalize
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FuncFormatter, MaxNLocator

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('roc_file', type=Path)
    parser.add_argument('-o', '--out-dir', type=Path, default='plots/roc2d')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-d', '--diagnostics',
                        choices={'delaunay','efficiency'},
                        default=set())
    parser.add_argument('-c', '--cache', type=Path,
                        **apd('cache/roc2d.h5'))
    parser.add_argument('-f', '--force-rebuild-cache', action='store_true')
    parser.add_argument('-l', '--draw-contours', action='store_true')
    parser.add_argument('-a', '--approved', action='store_true')
    return parser.parse_args()

def format_cb_value(value, pos):
    if abs(value - round(value)) < 0.001:
        return f'{value:.0f}'
    return f'{value:.1f}'


def integrate(hist):
    # If we have a lot of discrim bins we can drop some of them. We do
    # this by setting the stride on the slice. This is calculated to
    # be at least 1 and otherwise give between 100 and 200 bins.
    s = max(hist.shape[0] // 100, 1)
    hist = np.cumsum(hist[::-1,:], axis=0)[::-s,:]
    hist = np.cumsum(hist[:,::-1], axis=1)[:,::-1]
    return hist

def draw_2d_hist(hist, out_path, discrim_name, limits,
                 do_log=False, vrange=None, do_contours=False,
                 cb_label='Background Rejection Ratio vs DL1',
                 approved=False):

    # make the pt into TeV
    extent = (*limits[0], *(l * 1e-6 for l in limits[1]))

    args = dict(
        aspect='auto',
        origin='lower',
        extent=extent,
        interpolation='nearest',
        norm=LogNorm() if do_log else Normalize())
    if vrange:
        args['vmin'], args['vmax'] = vrange

    out_path.parent.mkdir(parents=True, exist_ok=True)
    cb_fmt = FuncFormatter(format_cb_value)

    with Canvas(out_path, fontsize=24) as can:
        add_atlas_label_in_box(can.ax, 0.05, 0.95, internal=(not approved))
        add_sim_info_in_box(can.ax, 0.05, 0.05)
        divider = make_axes_locatable(can.ax)
        cax = divider.append_axes("right", size="8%", pad=0.1)
        im = can.ax.imshow(hist.T, **args)
        can.ax.set_xlabel('Higgs Efficiency', **xlabdic(28))
        can.ax.set_ylabel(r'Minimum Large-$R$ Jet $p_{\rm T}$ [TeV]', **ylabdic(28))
        can.ax.xaxis.set_major_locator(MaxNLocator(steps=[1],prune='lower'))
        cb = can.fig.colorbar(im, cax=cax)
        if do_contours:
            cont = can.ax.contour(
                hist.T, levels=[2,5],
                linestyles='-', linewidths=10.0, alpha=0.5,
                origin='lower', extent=extent)
            cb.add_lines(cont)
        cb.set_label(cb_label, fontsize=28)
        cb.ax.yaxis.set_major_formatter(cb_fmt)
        cb.ax.yaxis.set_minor_formatter(cb_fmt)
        cb.ax.tick_params(labelsize=28,which='both')

def get_roc_interp(name, discrim, limits, signal='higgs', background='dijet',
                   diagnostics=set(),
                   out_dir=Path('test')):
    eff = integrate(discrim[signal])
    bg_eff = integrate(discrim[background])

    efmax = eff[1:,:].max(axis=0)
    v = efmax != 0
    eff[:,v] /= efmax[v]
    if 'efficiency' in diagnostics:
        draw_2d_hist(eff[1:,:], '.', 'effhist.pdf', discrim)

    efmax = bg_eff[1:,:].max(axis=0)
    v = efmax != 0
    bg_eff[:,v] /= efmax[v]

    # there N-1 edges, -2 for overflow, +1 because of fenceposting
    pt = np.linspace(*limits[1], eff.shape[1] - 1)
    # drop the underflow bins
    x, z = eff[1:,1:], bg_eff[1:,1:]
    y = np.meshgrid(x[:,0],pt)[1].swapaxes(0,1)
    if 'roc' in diagnostics:
        with Canvas(f'roc_{name}.pdf') as test:
            for band in [0, 1, 2]:
                test.ax.plot(x[:,band], 1 / z[:,band], label=f'{band}')
            test.ax.set_yscale('log')
            test.ax.legend()
    # build x, y values to interpolate over
    x_vs_y = np.stack((x, y), 2)
    flat_xy = x_vs_y.reshape(-1,2)
    yscale = flat_xy[:,1].ptp()
    flat_xy[:,1] /= yscale
    grid = Delaunay(flat_xy)
    if 'delaunay' in diagnostics:
        dpath = out_dir / f'points_{name}.pdf'
        print(f'drawing delaunay interp at {dpath}')
        with Canvas(dpath) as can:
            flatx, flaty = flat_xy[:,0], flat_xy[:,1]
            can.ax.scatter(flatx, flaty, c=np.log(z.flatten()) )
            can.ax.triplot(flatx, flaty, grid.simplices.copy())
    interp = LinearNDInterpolator(grid, z.flatten())
    xint, yint = np.linspace(0.4,1,501), np.linspace(pt[0], pt[-1], 501)
    xi = np.stack(np.meshgrid(xint, yint), 2).swapaxes(0,1)
    xi_scale = np.array(xi)
    xi_scale[:,:,1] /= yscale
    zi = interp(xi_scale)
    if 'scatter' in diagnostics:
        with Canvas(f'interp_{name}.pdf') as test:
            for pt in range(0, len(pt), len(pt) // 5):
                xs = xi[:, pt ,0]
                zs = zi[:, pt]
                v = ~np.isnan(zs)
                test.ax.scatter(xs[v], zs[v].max() / zs[v], label=f'{pt}')
            test.ax.legend()
            test.ax.set_yscale('log')
    label = r'$1 / \epsilon_{{\rm bg}}$ for {}'.format(
        DISCRIMINANT_NAME_MAP[name])
    draw_lims = [(l.min(), l.max()) for l in [xint, yint]]
    if 'efficiency' in diagnostics:
        draw_2d_hist(zi,out_path=out_dir/f'{name}.pdf',
                     limits=draw_lims,
                     cb_label=label,
                     discrim_name=name, do_log=False)
    return zi, draw_lims


def run():
    args = get_args()
    discrims = {}
    limits = {}
    helvetify()
    with File(args.roc_file, 'r') as rocs:
        for dname, discrim in rocs.items():
            lims = []
            discrims[dname] = {}
            for proc_name, ds in discrim.items():
                discrims[dname][proc_name] = np.asarray(ds['hist'])
                lims.append(ds.attrs['limits'])
            assert all((x == lims[0]).all() for x in lims)
            limits[dname] = lims[0]

    cache = args.cache
    cache.parent.mkdir(exist_ok=True)
    if args.force_rebuild_cache and cache.exists():
        cache.unlink()
    for baseline in ['mv2','dl1r']:
        out_dir = args.out_dir
        draw_2d_rocs(discrims, out_dir, limits, baseline=baseline,
                     diagnostics=args.diagnostics,
                     cache=args.cache, do_contours=args.draw_contours,
                     approved=args.approved)

def draw_2d_rocs(discrims, out_dir, limits, backgrounds=['dijet','top'],
                 baseline='mv2',
                 diagnostics=set(), cache=Path('cache/roc2d.h5'), **plotargs):
    if cache.exists():
        print(f'{cache} found, delete to rebuild')

    rocs = {}
    with File(cache,'a') as h5file:
        for name, discrim in discrims.items():
            for bg in backgrounds:
                objpath = f'{name}/higgs/{bg}'
                if objpath in h5file:
                    print(f'loading {objpath}')
                    rocs[(name,bg)] = np.asarray(h5file[objpath])
                    lims = h5file[objpath].attrs['lims']
                else:
                    print(f'building {name}, higgs / {bg}')
                    rocs[(name, bg)], lims = get_roc_interp(
                        name, discrim, limits[name], out_dir=out_dir/bg,
                        background=bg, diagnostics=diagnostics)
                    ds = h5file.create_dataset(objpath, data=rocs[(name,bg)])
                    ds.attrs['lims'] = lims

    ratio_discrim = 'Hbb_025'
    f_top = {'Hbb_025':0.25}[ratio_discrim]
    for bg in backgrounds:
        proc_name = PROCESS_NAME_MAP[bg]
        denom_name = DISCRIMINANT_NAME_MAP_SHORT[baseline]
        cb_label = (
            f'{proc_name} ratio: '
            rf'$\epsilon_{{\rm {denom_name}}} / '
            r'\epsilon_{D_{\rm Xbb}}$, $f_{\rm top} ='f' {f_top}'r'$'
        )
        draw_2d_hist(rocs[(baseline,bg)]/rocs[(ratio_discrim,bg)], limits=lims,
                     out_path=out_dir/'ratio'/baseline/f'{bg}.pdf',
                     discrim_name=name,
                     do_log=True, cb_label=cb_label, **plotargs)


if __name__ == '__main__':
    run()
