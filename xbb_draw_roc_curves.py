#!/usr/bin/env python3

"""
Draw Roc curves
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
import json, os
from itertools import product
from pathlib import Path
from collections import defaultdict

import matplotlib.pyplot as plt

from xbb.mpl import Canvas, CanvasWithRatio, log_style, add_atlas_label
from xbb.style import (
    DISCRIMINANT_NAME_MAP, DISCRIMINANT_NAME_MAP_SHORT,
    DISCRIMINANT_COLOR_MAP, DISCRIMINANT_LS_MAP, RATIO_ZOOM_MAP,
)
from xbb.mpl import xlabdic, add_kinematic_acceptance
from matplotlib.ticker import MaxNLocator

_discrim_default = ['Hbb_025', 'dl1r', 'mv2', 'mv2_FR']
_default_ratio = 'mv2'

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('h5_roc_curves')
    parser.add_argument(
        '-d', '--discriminant-list', nargs='*',
        metavar='DISCRIM',
        default=_discrim_default,
    )
    parser.add_argument('-o', '--out-dir', type=Path, default='plots/roc')
    parser.add_argument('-r', '--ratio', default=_default_ratio)
    parser.add_argument(
        '-b', '--benchmark-values-pct', nargs='+', type=int,
        metavar='PCT',
        help=(
            "write out benchmark rejection values alongside plots "
            "efficiency is listed in percent"
        ),
    )
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('-a','--approved', action='store_true')
    return parser.parse_args()

def run():
    args = get_args()
    benchmarks = {}
    with File(args.h5_roc_curves,'r') as h5file:
        for pt_range, pt_group in h5file.items():
            discrims = {}
            for discrim_name, group in pt_group.items():
                ds_names = ['higgs', 'dijet', 'top', 'edges']
                if args.discriminant_list:
                    if discrim_name not in args.discriminant_list:
                        continue
                roc_components = {
                    x: np.asarray(group[x]) for x in ds_names
                }
                discrims[discrim_name] = roc_components
            out_path = args.out_dir/pt_range
            out_path.mkdir(parents=True, exist_ok=True)
            if args.benchmark_values_pct:
                eff_values = [x / 100 for x in args.benchmark_values_pct]
                benchmarks[pt_range] = get_benchmark_values(
                    discrims, eff_values
                )
            else:
                pt_tuple = pt_group.attrs['pt_range']
                draw_roc_curves(discrims, out_dir=out_path,
                                approved=args.approved,
                                pt_range=pt_tuple, ratio=args.ratio,
                                debug=args.debug, pt_string=pt_range)

    if benchmarks:
        json_dir = args.out_dir/'json'
        json_dir.mkdir(parents=True, exist_ok=True)
        with open(json_dir/'rejections.json', 'w') as json_out:
            outdic = {}
            for pt_range, bm in benchmarks.items():
                outdic['pt' + pt_range] = json_vars(bm, baseline=args.ratio)
            json.dump(outdic, json_out, indent=2)


def json_vars(benchmarks, signals={'higgs'}, baseline=None):
    outdic = {}
    for signal in signals:
        defdict = defaultdict(dict)
        for (disc, sig, bg), bg_eff in benchmarks.items():
            if sig != signal or bg == signal:
                continue
            sig_effs = benchmarks[disc, signal, signal]
            defdict[disc][bg] = {}
            for eff, rej in zip(sig_effs*100, 1/bg_eff):
                defdict[disc][bg][f'Eff{eff:.0f}'] = rej
            if baseline:
                baseline_eff = benchmarks[baseline, signal, bg]
                for eff, ratio in zip(sig_effs*100, baseline_eff/bg_eff):
                    defdict[disc][bg][f'Eff{eff:.0f}_rel'] = ratio
        outdic[signal] = dict(defdict)
    return outdic

def get_benchmark_values(discrims, eff_values):
    benchmark_values = {}
    signals = ['higgs']
    for disc_name, hists in discrims.items():
        for signal in signals:
            signal_cum = hists[signal][::-1].cumsum()
            signal_cum /= signal_cum[-1]
            arb_cut_scale = np.arange(signal_cum.size)
            cut_values = np.interp(
                eff_values, signal_cum, arb_cut_scale)
            for bg_name, hist in hists.items():
                # the "edges" array should be ignored
                if bg_name == 'edges':
                    continue
                bg_cum = hist[::-1].cumsum()
                bg_cum /= bg_cum[-1]
                interp_eff = np.interp(cut_values, arb_cut_scale, bg_cum)
                benchmark_values[disc_name, signal, bg_name] = interp_eff
    return benchmark_values

def draw_roc_curves(discrims, pt_string, out_dir, **plot_args):
    discrims_higgs = {}
    discrims_top = {}
    for name, disc in discrims.items():
        if 'top_vs_qcd' in name:
            discrims_top[name] = disc
        else:
            discrims_higgs[name] = disc
    draw_roc_curves_multijet(discrims_higgs, out_dir=out_dir, **plot_args)
    zoom_outdir = out_dir/'zoom'
    zoom_range = RATIO_ZOOM_MAP.get((pt_string, 'multijet'))
    if zoom_range:
        draw_roc_curves_multijet(
            discrims_higgs, out_dir=zoom_outdir,
            zoom_range=zoom_range, **plot_args)
    draw_roc_curves_top(discrims_higgs, out_dir, **plot_args)
    zoom_range = RATIO_ZOOM_MAP.get((pt_string, 'top'))
    if zoom_range:
        draw_roc_curves_top(
            discrims_higgs, out_dir=zoom_outdir,
            zoom_range=zoom_range, **plot_args)
    if discrims_top:
        draw_top_tagging_roc_curves(discrims_top, **plot_args)

def set_ratio_style(ax, ratio, yrange=None):
    discrim = DISCRIMINANT_NAME_MAP_SHORT[ratio]
    ax.set_ylabel(f'Ratio to {discrim}', fontsize=20)
    if yrange is not None:
        ax.set_ylim(yrange)
    ax.yaxis.set_major_locator(MaxNLocator('auto',prune='upper'))

def legopts(approved):
    opts = dict(frameon=False, fontsize=16)
    if approved:
        opts['bbox_to_anchor'] = (1.0, 0.88)
    return opts

def draw_roc_curves_multijet(discrims, out_dir, pt_range, zoom_range=None,
                             ratio=None, debug=False, approved=False,
                             rejmax=None):
    Can = CanvasWithRatio if ratio else Canvas
    with Can(out_dir/'dijet.pdf') as can:
        log_style(can.ax)
        add_atlas_label(can.ax, internal=(not approved))
        add_kinematic_acceptance(can.ax, pt_range)
        if ratio:
            baseline_sig, baseline_rej = discrims[ratio]['higgs'], discrims[ratio]['dijet']
        else: baseline_sig, baseline_rej = np.array([]),np.array([])
        for dis_name, discrims in discrims.items():
            sig, bg = discrims['higgs'], discrims['dijet']
            draw_roc(can, sig, bg, out_dir, baseline_sig, baseline_rej,
                     label=dis_name, debug=debug)
        can.ax.set_ylabel('Multijet Rejection',fontsize=20)
        can.ax.set_ylim(None,rejmax or 2e3)
        can.ax.legend(**legopts(approved))
        if isinstance(can, CanvasWithRatio):
            set_ratio_style(can.ax2, ratio, yrange=zoom_range)
            can.ax2.set_xlabel('Higgs Efficiency', **xlabdic())
        else: can.ax.set_xlabel('Higgs Efficiency', **xlabdic())

def draw_roc_curves_top(discrims, out_dir, pt_range, zoom_range=None,
                        ratio=None, debug=False, approved=False,
                        rejmax=None):
    Can = CanvasWithRatio if ratio else Canvas
    with Can(out_dir/'top.pdf') as can:
        log_style(can.ax)
        add_atlas_label(can.ax, internal=(not approved))
        add_kinematic_acceptance(can.ax, pt_range)
        if ratio: baseline_sig, baseline_rej = discrims[ratio]['higgs'], discrims[ratio]['top']
        else: baseline_sig, baseline_rej = np.array([]),np.array([])
        for dis_name, discrims in discrims.items():
            sig, bg = discrims['higgs'], discrims['top']
            draw_roc(can, sig, bg, out_dir, baseline_sig, baseline_rej,
                     label=dis_name, debug=debug)
        can.ax.set_ylabel('Top Rejection',fontsize=20)
        can.ax.set_ylim(None,rejmax or 1e3)
        can.ax.legend(**legopts(approved))
        if isinstance(can, CanvasWithRatio):
            set_ratio_style(can.ax2, ratio, yrange=zoom_range)
            can.ax2.set_xlabel('Higgs Efficiency',**xlabdic())
        else: can.ax.set_xlabel('Higgs Efficiency',**xlabdic())

def draw_top_tagging_roc_curves(discrims, out_dir, pt_range,
                                zoom_range, ratio, debug, approved=False):
    do_ratio = ratio and ratio in discrims
    Can = CanvasWithRatio if do_ratio else Canvas
    with Can(out_dir/'top_tagging.pdf') as can:
        add_kinematic_acceptance(can.ax, pt_range)
        add_atlas_label(can.ax, internal=(not approved))
        log_style(can.ax)
        if do_ratio:
            baseline_sig, baseline_rej = discrims[ratio]['top'], discrims[ratio]['dijet']
        else: baseline_sig, baseline_rej = np.array([]),np.array([])
        for dis_name, discrims in discrims.items():
            baseline_sig, baseline_rej = np.array([]),np.array([])
            sig, bg = discrims['top'], discrims['dijet']
            draw_roc(can, sig, bg, out_dir, baseline_sig, baseline_rej,
                     label=dis_name, debug=debug)
        can.ax.set_ylabel('Multijet Rejection',fontsize=20)
        can.ax.legend(frameon=False,fontsize=16)
        if isinstance(can, CanvasWithRatio):
            can.ax2.set_ylabel('Ratio to %s'%ratio.upper(),fontsize=20)
            can.ax2.set_xlabel('Top Efficiency',**xlabdic())
        else: can.ax.set_xlabel('Top Efficiency',**xlabdic())


def get_disc_name(raw):
    l = raw.lower()
    if raw in DISCRIMINANT_NAME_MAP:
        return DISCRIMINANT_NAME_MAP[raw]
    return raw.upper()

def draw_roc(canvas, sig, bg, out_dir, baseline_sig, baseline_bg,
             label, min_eff=0.4, debug=False):

    eff = np.cumsum(sig[1:-1][::-1])[::-1] # numerator only, excluding overflow and underflow
    denom = (np.cumsum(sig[::-1])[::-1]).max() # denominator, including overflow and underflow
    eff /= denom
    bg_eff = np.cumsum(bg[1:-1][::-1])[::-1] # numerator only, excluding overflow and underflow
    bg_denom = np.cumsum(bg[::-1])[::-1].max() # denominator, including overflow and underflow
    bg_eff /= bg_denom

    rej = np.zeros_like(bg_eff)
    valid = bg_eff > 0.0
    rej[valid] = 1/bg_eff[valid]

    xbins = np.arange(sig[1:-1].size)

    valid_eff = eff > min_eff
    fullname = get_disc_name(label)
    lines = canvas.ax.plot(
        eff[valid_eff], rej[valid_eff], label=fullname,
        color=DISCRIMINANT_COLOR_MAP[label],linestyle=DISCRIMINANT_LS_MAP[label],linewidth=3)

    # set the bottom of the plot to be 1, turn off ticks
    canvas.ax.set_ylim(bottom=1.0)
    canvas.ax.tick_params(bottom=False, which='both')

    if isinstance(canvas, CanvasWithRatio):

        baseline_eff, baseline_rej = get_baseline_rejection(baseline_sig,baseline_bg,min_eff)
        this_rej = lines[0].get_ydata()
        this_eff = lines[0].get_xdata()

        from scipy import interpolate
        f = interpolate.interp1d(baseline_eff, baseline_rej,
                                 bounds_error=False)
        interp_baseline_rej = f(this_eff)

        # define ratio to plot
        ratio_of_rejections = np.ones_like(interp_baseline_rej)

        ratio_of_rejections = this_rej/interp_baseline_rej
        canvas.ax2.plot(
            this_eff,ratio_of_rejections,label=fullname,
            color=DISCRIMINANT_COLOR_MAP[label],linestyle=DISCRIMINANT_LS_MAP[label],linewidth=3)

    if not debug:
        return

    if debug:
        debug_dir = out_dir/'debug'
        debug_dir.mkdir(exist_ok=True)
        with Canvas(debug_dir/f'{label}.pdf') as can:
            can.ax.step(xbins, eff, label='signal')
            can.ax.step(xbins, bg_eff, label='bg')
            can.ax.legend()

def get_baseline_rejection(sig,bg,min_eff):

    eff = np.cumsum(sig[1:-1][::-1])[::-1] # numerator only, excluding overflow and underflow
    denom = (np.cumsum(sig[::-1])[::-1]).max() # denominator, including overflow and underflow
    eff /= denom
    bg_eff = np.cumsum(bg[1:-1][::-1])[::-1] # numerator only, excluding overflow and underflow
    bg_denom = np.cumsum(bg[::-1])[::-1].max() # denominator, including overflow and underflow
    bg_eff /= bg_denom

    rej = np.zeros_like(bg_eff)
    valid = bg_eff > 0.0
    rej[valid] = 1/bg_eff[valid]

    valid_eff = eff > min_eff
    lines = plt.plot(eff[valid_eff], rej[valid_eff])

    return lines[0].get_xdata(),lines[0].get_ydata()

if __name__ == '__main__':
    run()
