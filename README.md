Hbb Training Dataset Validation Scripts
=======================================

This repository contains several scripts to verify that the training
datsets for H->bb tagging are doing what we think they should be
doing. Since several stages are required to properly assign weights to
the samples, it's important that a few scripts be run first.

 - To calculate the right normalization for the dijet samples, you
   have to run `get_weight_denominator.py`. Note that this _only_
   needs to be run over the dijet samples. This will produce a
   "denominator" json file that the other scripts require.

 - To calculate the right normalization for the higgs samples, you
   need to run `make_jet_pt_hist.py`. This will produce a few files in
   `pt-hists` which are used to scale the higgs jet pt spectrum to
   match the dijet spectrum.

 - After these steps are run you can run `make_roc_curves.py` which
   will plot a few roc curves for the baseline H->bb tagging
   discriminants.


Step-by-step instructions for ROC curves
==========================================
Using as an example the mc16a xbb training samples listed in [xbb-datasets](https://gitlab.cern.ch/atlas-boosted-hbb/xbb-datasets/).

1. Create symlinks with shorter and user-friendly names
```
./xbb_rename.py xbb-datasets/p3990/mc16a-input-datasets.txt xbb-datasets/p3990/mc16a-hbb-datasets.txt <sample_location> -o <folder_renamed_samples>
```

2. Determine normalization for dijet samples:
```
./xbb_get_weight_demominator.py -o <folder_renamed_samples>/denom.json <folder_renamed_samples>/mc16a.*
```

3. For sample pt reweighting:
```
./xbb_make_jet_pt_hist.py -d <folder_renamed_samples>/denom.json -x data/xsec.txt <folder_renamed_samples>/mc16a.* -o <pt_hists>
```

4. Make roc curve (define discriminator/tagger in `xbb_make_roc_curves.py`):
```
./xbb_make_roc_curves.py -i <pt_hists> -x data/xsec.txt -d <folder_renamed_samples>/denom.json -s <h5_roc_tagger>.h5 <folder_renamed_samples>/mc16a.*
```

5. (optional) Combine roc curves of all discriminators into single h5 file:
```
h5copy -i <h5_roc_tagger>.h5 -o roc_combined.h5 -s <tagger> -d <tagger>
```

6. Draw roc curves with ratio panel (discriminator in denominator determined by `-r`):
```
./xbb_draw_roc_curves.py roc_combined.h5 -d <discrim1> <discrim2> -r <discrim1>
```


Step-by-step instructions for histograms
==========================================
Assuming steps 1-3 of the above instruction have been run:

1. Produce histograms from chosen discriminant from DISCRIMINANT_GETTERS (e.g. --var dl1r)
```
./xbb_make_disc_hist.py -d <folder_renamed_samples>/denom.json -x data/xsec.txt <folder_renamed_samples>/mc16a.* -o <out_hists> --var <var_name>
```

2. Draw:
```
./xbb_draw_hists.py <out_hists>/<var_name>.h5 -o <out_hists> --var <var_name>
```




Example instructions for PUB note plots
=======
* Figure 1
```
./xbb_make_jet_pt_hist.py -v -d mc16d_Hbb_March2020-renamed/denom.json -x data/xsec.txt slimmed/mc16d.* -o plots_mc16d
python xbb_draw_hists.py plots_mc16d/jetpt.h5 --var pt -o plots_mc16d
```

* Figure 2
```
python xbb_make_disc_hist.py -d mc16d_Hbb_March2020-renamed/denom.json -x data/xsec.txt slimmed/mc16d.* -o plots_mc16d --var dl1r -v

python xbb_make_disc_hist.py -d mc16d_Hbb_March2020-renamed/denom.json -x data/xsec.txt slimmed/mc16d.* -o plots_mc16d --var Hbb_025 -v

python xbb_draw_hists.py plots_mc16d/Hbb_025.h5 -o plots_mc16d --var Hbb_025 -p dijet higgs top
python xbb_draw_hists.py plots_mc16d/dl1r.h5 -o plots_mc16d --var dl1r -p dijet higgs top

```
