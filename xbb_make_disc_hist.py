#!/usr/bin/env python3

"""
Make histograms of discriminants
"""

from argparse import ArgumentParser, ArgumentTypeError
from h5py import File
from glob import glob
import numpy as np
import json, os
from sys import stderr
from pathlib import Path

from xbb.common import get_denom_dict, get_dsid, get_name, get_pretty_name
from xbb.common import SELECTORS
from xbb.common import is_dijet, is_dihiggs, is_wz
from xbb.cross_section import CrossSections
from xbb.selectors import (
    truth_match,
    EVENT_LABELS,
    all_events,
    rm_large_weights,
    truth_match_mass_window_higgs,
    mass_window_higgs
)
from xbb_make_roc_curves import DISCRIMINANT_GETTERS, DISCRIMINANT_EDGES



def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('datasets', nargs='+')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-d', '--denominator', required=True)
    parser.add_argument('-x', '--cross-sections', required=True)
    parser.add_argument('-c', '--cut-thresholds', nargs='*',
                        default=[],
                        type=threshold_type, help=_threshold_help)
    parser.add_argument('-p', '--pt-dir', default='pt-hists', type=Path)
    parser.add_argument('-o', '--output-file', default='discrims.h5',
                        type=Path)
    parser.add_argument('--var', default='dl1r')
    parser.add_argument('-f', '--force', action='store_true',
                        help='delete output if it exists')
    return parser.parse_args()

_threshold_help = 'Expects VAR=THRESHOLD pair'
def threshold_type(arg):
    if '=' not in arg:
        raise ArgumentTypeError(
            'cut thresholds should be specified as VAR=THRESHOLD')
    try:
        var, threshold_string = arg.split('=')
        threshold = float(threshold_string)
    except:
        raise ArgumentTypeError(f"Couldn't parse {arg}")
    return var, threshold

def get_hist(ds, var, edges, selection=all_events, ds_wt=1.0, reweight=False, ratio_pt=None, edges_pt=None):
    hist = 0
    for fpath in glob(f'{ds}/*.h5'):
        with File(fpath,'r') as h5file:
            disc = DISCRIMINANT_GETTERS[var]
            d = disc(h5file)
            weight = h5file['fat_jet']['mcEventWeight']
            if reweight:
                pt = h5file['fat_jet']['pt']
                indices = np.digitize(pt, edges_pt) - 1
                weight = ratio_pt[indices]
            mega_weights = np.array(weight, dtype=np.longdouble) * ds_wt
            sel_index = selection(h5file)
            hist += np.histogram(
                d[sel_index], edges, weights=mega_weights[sel_index])[0]
            if np.any(np.isnan(hist)):
                stderr.write(f'{fpath} has nans\n')
    return hist

def save_hist(hist, edges, group):
    # we need a bit of a hack here: float128 (longdouble) doesn't
    # get stored properly by h5py as of version 2.8
    group.create_dataset('hist', data=hist, dtype=float, compression='gzip')
    group.create_dataset('edges', data=edges, compression='gzip')

def run():
    args = get_args()
    if args.output_file.exists():
        if args.force:
            args.output_file.unlink()
    edges = DISCRIMINANT_EDGES[args.var]
    n_bins = 25
    if edges.size > n_bins:
        edges = np.linspace(edges.min(), edges.max(), n_bins)

    selections = []
    for var, threshold in args.cut_thresholds:
        if var in DISCRIMINANT_GETTERS:
            getter = DISCRIMINANT_GETTERS[var]
            def cut(ds):
                return getter(ds) > threshold
        else:
            def cut(ds):
                return ds['fat_jet'][var] > threshold

        selections.append((var, cut, threshold))

    for var, cut, threshold in selections + [('all', select_all, -np.inf)]:
        cutstr = 'none' if threshold == -np.inf else f'{threshold:.2f}'
        args.output_file.parent.mkdir(parents=True, exist_ok=True)
        with File(args.output_file,'a') as output:
            outgroup_str = f'{var}/{args.var}/{cutstr}'
            if outgroup_str in output:
                print(f'{outgroup_str} exists, not remaking')
                continue
            outgroup = output.create_group(outgroup_str)
            outgroup.attrs['threshold'] = threshold
            common=dict(common_selector=cut, group=outgroup)
            run_sample_reweighted(edges, 'higgs', args, **common)
            run_sample_reweighted(edges, 'top', args, **common)
            run_dijet(edges, args, **common)

def select_all(ds):
    return np.ones(ds['fat_jet'].shape, dtype=bool)

def run_dijet(edges, args, group, common_selector=select_all):
    with open(args.denominator, 'r') as denom_file:
        denom = get_denom_dict(denom_file)
    with open(args.cross_sections, 'r') as xsec_file:
        xsecs = CrossSections(xsec_file, denom)

    def selection(ds):
        return common_selector(ds) & mass_window_higgs(ds['fat_jet'])

    hist = 0
    for ds in args.datasets:
        dsid = get_dsid(ds)
        if not is_dijet(dsid, restricted=True):
            continue
        if args.verbose:
            print(f'processing {ds} as dijet')
        if xsecs.datasets[dsid]['denominator'] == 0:
            continue
        weight = xsecs.get_weight(dsid)

        this_dsid = get_hist(ds, args.var, edges,
                             selection=selection, ds_wt=weight)
        name = get_name(ds)
        hist += this_dsid

    sample_group = group.create_group('dijet')
    save_hist(hist, edges, sample_group)


def run_sample_reweighted(edges, process, args, group,
                          common_selector=select_all):
    hist = 0
    selector_base = truth_match_mass_window_higgs(EVENT_LABELS[process])
    def selector(ds):
        return selector_base(ds['fat_jet']) & common_selector(ds)
    with File(args.pt_dir/'jetpt.h5', 'r') as h5file:
        num = h5file['dijet']['hist']
        denom = h5file[process]['hist']
        ratio = np.zeros_like(num)
        edges_pt = h5file['dijet']['edges'][:]
        valid = np.asarray(denom) > 0.0
        ratio[valid] = num[valid] / denom[valid]


    for ds in args.datasets:
        dsid = get_dsid(ds)
        if not SELECTORS[process](dsid, restricted=True):
            continue
        if args.verbose:
            print(f'processing {ds} as {process}')

        this_dsid = get_hist(ds, args.var, edges, selection=selector, ds_wt=1.0, reweight=True, ratio_pt=ratio, edges_pt=edges_pt)
        name = get_name(ds)
        hist += this_dsid

    sample_group = group.create_group(process)
    save_hist(hist, edges, sample_group)

if __name__ == '__main__':
    run()
