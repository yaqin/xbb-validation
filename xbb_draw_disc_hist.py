#!/usr/bin/env python3

"""
Make histograms of discriminants
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict
from xbb.style import (
    DISCRIMINANT_NAME_MAP,
    DISCRIMINANT_COLOR_MAP,
    PROCESS_COLOR_MAP,
    PROCESS_NAME_MAP,
)

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('input_file', type=Path)
    parser.add_argument(
        '-o', '--out-dir', default='plots/disc', type=Path, help=d)
    parser.add_argument('-e', '--extension', default='.pdf', help=d)
    return parser.parse_args()

def draw_hists(parts, edges, var, out_path, ylab='Jets'):
    from xbb.mpl import Canvas, xlabdic, ylabdic, helvetify, sad_atlas_label
    helvetify()

    out_path.parent.mkdir(parents=True, exist_ok=True)
    centers = (edges[1:] + edges[:-1]) / 2
    with Canvas(out_path) as can:
        sad_atlas_label(can.ax, x=0.05, y=0.95, vshift=0.05)
        can.ax.set_ylabel(f'{ylab} (normalized)', **ylabdic())
        can.ax.set_xlabel( DISCRIMINANT_NAME_MAP[var], **xlabdic())
        for (name, cut), (hist, color) in parts.items():
            label = PROCESS_NAME_MAP.get(name)
            if not label:
                label = DISCRIMINANT_NAME_MAP.get(name,name)
            if cut != -np.inf:
                label = f'({label})' r' $ > ' f'{cut:.2f}' r'$'
            can.ax.plot(centers, hist / hist.sum(), label=label,
                        color=color, linewidth=3)
        maxval = can.ax.get_ylim()[1]
        can.ax.set_ylim(0, maxval*1.3)
        can.ax.legend(fontsize=16, frameon=False)

def histiter(h5file, out_dir):
    for selection, vargroup in h5file.items():
        basepath = out_dir / 'by_process'
        for var, cutgroup in vargroup.items():
            for cut, procgroup in cutgroup.items():
                cutval = procgroup.attrs['threshold']
                path = basepath / selection / var / cut
                yield selection, var, cutval, procgroup, path

def run():
    args = get_args()
    edges = None
    proc_var_by_sel = defaultdict(dict)
    with File(args.input_file,'r') as hf:
        for sel, var, cut, procgroup, outpath in histiter(hf, args.out_dir):
            parts = {}
            for process, histgroup in procgroup.items():
                hist = np.asarray(histgroup['hist'])
                parts[process,cut] = hist, PROCESS_COLOR_MAP[process]
                proc_var_by_sel[process, var][sel,cut] = (
                    hist, DISCRIMINANT_COLOR_MAP[sel])
                this_edges = np.asarray(histgroup['edges'])
                if edges is None:
                    edges = this_edges
                else:
                    if not np.allclose(edges, this_edges):
                        raise Exception('edges incompatable')

            draw_hists(
                parts, edges, var, outpath.with_suffix(args.extension))

    for (process, variable), parts in proc_var_by_sel.items():
        path = args.out_dir / 'by_selection' / process / variable
        y_label = PROCESS_NAME_MAP[process] + ' Jets'
        draw_hists(parts, edges, variable, path.with_suffix(args.extension),
                   ylab=y_label)

if __name__ == '__main__':
    run()
