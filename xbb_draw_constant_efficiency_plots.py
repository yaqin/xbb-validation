#!/usr/bin/env python3

"""
Draw background rejection plots for constant efficiency.

This uses the outputs from xbb_make_2d_roc.py

"""

from argparse import ArgumentParser
from pathlib import Path
from collections import defaultdict

import numpy as np
from h5py import File
from matplotlib.ticker import MaxNLocator
from matplotlib.colors import to_rgba

from xbb.mpl import Canvas, helvetify, xlabdic, ylabdic, log_style
from xbb.mpl import add_kinematic_acceptance, add_atlas_label
from xbb.style import DISCRIMINANT_NAME_MAP, PROCESS_NAME_MAP
from xbb.style import DISCRIMINANT_COLOR_MAP, DISCRIMINANT_LS_MAP

def get_args():
    parser = ArgumentParser(description=__doc__)
    d = 'default: %(default)s'
    parser.add_argument('roc_file', type=Path)
    parser.add_argument('-l', '--log', action='store_true')
    parser.add_argument('-e', '--errorbar', action='store_true')
    parser.add_argument('-o', '--out-dir', type=Path,
                        default='plots/eff_vs_pt')
    parser.add_argument('-a','--approved', action='store_true')
    return parser.parse_args()

def format_cb_value(value, pos):
    return f'{value:.0f}'

def get_rebinned(name, discrim, limits):
    pt_bounds = limits[1]

    # -2 for overflow, +1 for fenceposting
    n_old_pt_edges = discrim.shape[1] - 1
    # aim for 2 bins every 250 GeV
    n_pt_edges = int((pt_bounds[1] - pt_bounds[0]) // 250e3 * 2) + 1

    old_bounds = np.linspace(*pt_bounds, n_old_pt_edges)
    new_bounds = np.linspace(*pt_bounds, n_pt_edges)
    assert np.isin(new_bounds, old_bounds).all()
    bin_map = np.concatenate(
        [[0],np.digitize(old_bounds, new_bounds)]
    )
    sums = []
    for x in sorted(set(bin_map)):
        sum_slice = discrim[:,np.where(bin_map == x)[0]].sum(axis=1)
        sums.append(sum_slice)
    return np.stack(sums, axis=1), new_bounds

def run():
    args = get_args()
    helvetify()
    for eff in [0.5, 0.6, 0.7, 0.8]:
        make_plots_for_eff(roc_file=args.roc_file,
                           eff=eff, log=args.log, out_dir=args.out_dir,
                           errorbar=args.errorbar, approved=args.approved)

def make_plots_for_eff(roc_file, eff, signal='higgs', **plot_args):
    discrims = {}
    limits = {}
    with File(roc_file, 'r') as rocs:
        for dname, discrim in rocs.items():
            lims = []
            discrims[dname] = {}
            for proc_name, ds in discrim.items():
                discrims[dname][proc_name] = (
                    np.asarray(ds['hist']), np.asarray(ds['wt2'])
                )
                lims.append(ds.attrs['limits'])
            assert all((x == lims[0]).all() for x in lims)
            limits[dname] = lims[0]

    rebinned = {}
    for name, discrim in discrims.items():
        for sample_name, hists in discrim.items():
            rebinned_hists = []
            for hist in hists:
                rebin, pt_edges = get_rebinned(name, hist, limits[name])
                rebinned_hists.append(rebin)
            rebinned[(name,sample_name)] = (*rebinned_hists, pt_edges)

    def is_signal(key):
        return key[0][1] == signal

    wp_masks = {}
    for key, hist in filter(is_signal,rebinned.items()):
        wp_masks[key] = get_mask(hist[0], eff=eff)

    efficiencies = {}
    for (name,samp), (hist, wt2, pt_edges) in rebinned.items():
        mask = wp_masks[(name,signal)]
        eff_hist, eff_error, pt_mask = get_efficiency(hist, wt2, mask)
        efficiencies[(name,samp)] = dict(
            hist=eff_hist, pt_mask=pt_mask, pt_edges=pt_edges, discrim=name,
            eff_error=eff_error)

    eff_by_plot = defaultdict(list)
    for (name, sample), hist_dict in efficiencies.items():
        eff_by_plot[sample].append(hist_dict)

    for sample_name, plot_list in eff_by_plot.items():
        draw_plot(plot_list=plot_list, eff_cut=eff, sample_name=sample_name,
                  **plot_args)


# hand-optimized ranges (delete to get auto range)
SAMPLE_RANGES_LOG = {
    ('dijet', 60): (4, 800),
    ('top'  , 60): (2, 200),
    ('higgs', 60): (1, 5),
}
SAMPLE_RANGES = {
    ('dijet', 50): (1, 200),
    ('top',   50): (1, 100),
    ('dijet', 60): (1, 150),
    ('top',   60): (1, 45),
    ('dijet', 70): (1, 90),
    ('top',   70): (1, 20),
}

def draw_fill_between(ax, x, eff, eff_error, color, style,
                      full_name, **ignore):
    ax.plot(x, 1/eff,
            label=full_name, color=color, linestyle=style,
            linewidth=3)
    ax.fill_between(
        x,
        1/(eff-eff_error),
        1/(eff+eff_error),
        color=color, alpha=0.1)

def draw_errorbar(ax, x, eff, eff_error, color, style, full_name, xerr):
    ebs = np.stack([
        -(1/(eff+eff_error) - 1/eff),
        +(1/(eff-eff_error) - 1/eff),
    ])

    # set a few things custom for dotted lines
    light_color = to_rgba(color, 0.6)
    bar_color = color
    markerfacecolor = None
    markeredgecolor = color
    linewidth = 4
    marker = 'o'
    markeredgewidth = 1
    if style == '--':
        linewidth = 2
        markeredgewidth = 2
        markerfacecolor = 'none'
        bar_color = color
        marker = 'o'

    ax.errorbar(x, 1/eff, ebs, xerr, marker=marker, markersize=10,
                label=full_name, color=bar_color, linestyle='none',
                markerfacecolor=markerfacecolor,
                markeredgecolor=markeredgecolor,
                markeredgewidth=markeredgewidth,
                linewidth=linewidth)


def draw_plot(plot_list, out_dir, eff_cut, sample_name, log=False,
              errorbar=False, approved=False):
    eff_pct = int(eff_cut * 100)
    eff_name = f'const_eff_{eff_pct:.0f}'
    file_name = f'{sample_name}_log.pdf' if log else f'{sample_name}.pdf'
    dirpath = out_dir/eff_name
    dirpath.mkdir(exist_ok=True, parents=True)
    with Canvas(dirpath/file_name, fontsize=24) as can:
        for pdic in plot_list:
            name = pdic['discrim']
            hist = pdic['hist']
            edges_mev = pdic['pt_edges']
            mask = pdic['pt_mask'][1:-1]
            edges = edges_mev * 1e-6
            centers = (edges[:-1] + edges[1:]) / 2
            full_name = DISCRIMINANT_NAME_MAP[name]
            color = DISCRIMINANT_COLOR_MAP[name]
            style = DISCRIMINANT_LS_MAP[name]
            x, eff = centers[mask], hist[1:-1][mask]
            xerr = np.stack([
                x - edges[:-1][mask], edges[1:][mask] - x])
            eff_error = pdic['eff_error'][1:-1][mask]
            plot = draw_errorbar if errorbar else draw_fill_between
            plot(
                can.ax,
                x=x, eff=eff, eff_error=eff_error,
                color=color, style=style,
                full_name=full_name,
                xerr=xerr)
        procname = PROCESS_NAME_MAP[sample_name]
        ylab = f'{procname} Rejection'
        can.ax.set_ylabel(ylab, ylabdic(28))
        can.ax.yaxis.set_major_locator(MaxNLocator(prune='upper'))
        lower_bound = edges[0] if errorbar else centers[0]
        xval = centers < 2.1
        upper_bound = (edges[1:] if errorbar else centers)[xval].max()
        can.ax.set_xlim(lower_bound, upper_bound)
        can.ax.set_xlabel(r'Large-$R$ Jet $p_{\rm T}$ [TeV]',**xlabdic(28))
        add_kinematic_acceptance(can.ax, eff=eff_cut, offset=0.05)
        if log:
            log_style(can.ax)
        hand_ranges = SAMPLE_RANGES_LOG if log else SAMPLE_RANGES
        if (sample_name, eff_pct) in hand_ranges:
            can.ax.set_ylim(*hand_ranges[sample_name, eff_pct])
        add_atlas_label(can.ax, vshift=0.05, internal=(not approved),
                        label_break_ratio_hack=0.8)
        can.ax.legend(frameon=False, fontsize=16)

def get_efficiency(hist, wt2, mask):
    total = hist.sum(axis=0)
    total_err = wt2.sum(axis=0)
    tagged = hist.sum(axis=0, where=mask)
    tagged_err = wt2.sum(axis=0, where=mask)
    untagged = total - tagged
    untagged_err = total_err - tagged_err

    # If the masked discriminant sum is entirely in the discriminant
    # underflow, it means we've lost the ability to adjust the cut
    # value. We want to mask these bins out when plotting.
    pt_mask = mask.shape[0] - mask.sum(axis=0) > 1

    # We define pt bins with no entries to have NaN eff
    eff = np.full_like(total, np.nan)
    eff_error = np.full_like(total, np.nan)
    nz = total > 0

    # efficinecy
    eff[nz] = tagged[nz] / total[nz]

    # compute the error
    #
    # this is just linear error propigateion
    #
    delta_tagged = tagged_err**(1/4)
    delta_untagged = untagged_err**(1/4)
    eff_error[nz] = (
        delta_tagged[nz] * (1 / total[nz] + tagged[nz] / total[nz]**2 )
        + delta_untagged[nz] * tagged[nz] / total[nz]**2
    )
    return eff, eff_error, pt_mask


def get_mask(hist, eff):
    integrated = hist[::-1,:].cumsum(axis=0)[::-1,:]
    # Only consider bins with some entries to calculate
    # efficiency. This leaves the pt bins with zero entries at zero
    # efficiency, which in turn means they are kept in the mask.
    zero = integrated[0,:] == 0
    integrated[:,~zero] /= integrated[0,~zero]
    mask = integrated < eff
    # we might as well mask off the pt bins with zero entries
    mask[:,zero] = False
    return mask

if __name__ == '__main__':
    run()
