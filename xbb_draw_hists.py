#!/usr/bin/env python3

"""
Draw Pretty Histograms by class (Top, QCD, Higgs, ...)
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
import os
from xbb.common import get_pretty_name
from xbb.style import DISCRIMINANT_NAME_MAP
from xbb.mpl import Canvas, CanvasWithRatio, log_style
from xbb.mpl import xlabdic, add_sim_info_in_box, add_kinematic_acceptance
from xbb.mpl import add_atlas_label

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('hists')
    parser.add_argument('-o', '--out-dir', default='plots')
    parser.add_argument('--var', default='dl1r')
    parser.add_argument('-p', '--processes', nargs='*')
    return parser.parse_args()

def run():
    args = get_args()
    discrims = {}
    out_dir = args.out_dir
    var = args.var
    processes = args.processes
    with File(args.hists,'r') as h5file:
        draw_hist(h5file,out_dir,var,processes)

def draw_hist(h5file,out_dir,var,list_processes):
    from xbb.mpl import Canvas, xlabdic, ylabdic, helvetify
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)

    with Canvas(f'{out_dir}/{var}.pdf') as can:
        add_atlas_label(can.ax, x=0.05, y=0.95, internal=True, size=28, vshift=0.05, add_kinematic_info=True)
        for process, group in h5file.items():
            if process not in list_processes: continue
            hist, edges = group['hist'], group['edges']
            if var == "pt":
                centers = 1e-6 * (edges[1:] + edges[:-1]) / 2
                gev_per_bin = (centers[2] - centers[1]) * 1e3
            else: centers = (edges[1:] + edges[:-1]) / 2
            can.ax.plot(centers, hist, label=get_pretty_name(process,'process'),linewidth=3)
        can.ax.grid(True)
        can.ax.set_ylabel('Arbitrary Units', **ylabdic())
        if var in DISCRIMINANT_NAME_MAP: name = DISCRIMINANT_NAME_MAP[var]
        else: name = var
        can.ax.set_xlabel(name, **xlabdic())
        maxval = can.ax.get_ylim()[1]
        minval = can.ax.get_ylim()[0]
        can.ax.set_ylim(None,maxval*1.2)
        if var == "pt": 
            can.ax.set_yscale('log')
            can.ax.set_ylabel(f'Jets / {gev_per_bin:.0f} GeV', **ylabdic())
            can.ax.set_xlabel(r'Fat Jet $p_{\rm T}$ [TeV]', **ylabdic())
            can.ax.set_xlim([0.25, 3])
            can.ax.set_ylim(None,maxval*70)
        can.ax.legend(loc='upper right',frameon=False,fontsize=22)


if __name__ == '__main__':
    run()
